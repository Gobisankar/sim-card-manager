//
//  HomeVC.swift
//  SimCardManager
//
//  Created by Humworld Solutions Private Limited on 26/10/21.
//

import UIKit

enum SimCardStatus: String {
    case unknown
    case suspended
    case active
    case inactive //means => new sim card
}

extension UIColor {
    func HexToColor(hexString: String, alpha:CGFloat? = 1.0) -> UIColor {
        // Convert hex string to an integer
        let hexint = Int(self.intFromHexString(hexStr: hexString))
        let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
        let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
        let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
        let alpha = alpha!
        // Create color object, specifying alpha as well
        let color = UIColor(red: red, green: green, blue: blue, alpha: alpha)
        return color
    }
    
    func intFromHexString(hexStr: String) -> UInt64 {
        var hexInt: UInt64 = 0
        // Create scanner
        let scanner: Scanner = Scanner(string: hexStr)
        // Tell scanner to skip the # character
        scanner.charactersToBeSkipped = NSCharacterSet(charactersIn: "#") as CharacterSet
        // Scan hex value
        scanner.scanHexInt64(&hexInt)
        return hexInt
    }
}

extension UIView {
    func addDashedBorder(dashWidth: CGFloat, dashColor: UIColor, dashLength: Int = 5, betweenDashesSpace: Int = 3, cornerRadius: CGFloat = 20) {
        let dashBorder = CAShapeLayer()
        dashBorder.lineWidth = dashWidth
        dashBorder.strokeColor = dashColor.cgColor
        dashBorder.lineDashPattern = [dashLength, betweenDashesSpace] as [NSNumber]
        dashBorder.frame = bounds
        dashBorder.fillColor = nil
        self.layer.cornerRadius = cornerRadius
        if cornerRadius > 0 {
            dashBorder.path = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius).cgPath
        } else {
            dashBorder.path = UIBezierPath(rect: bounds).cgPath
        }
        self.layer.addSublayer(dashBorder)
    }
}

extension UILabel {
    func monoChromaticLabel(textColor: UIColor, backgroundColor: UIColor) {
        self.backgroundColor = backgroundColor
        self.textColor = textColor
        self.layer.cornerRadius = self.frame.height/2
        self.layer.masksToBounds = true
    }
    
    func activeLabel(text: String = "Active") {
        self.text = text
        self.monoChromaticLabel(textColor: UIColor().HexToColor(hexString: "#228733"), backgroundColor: UIColor().HexToColor(hexString: "#d1f3d7"))
    }
    
    func suspendedLabel(text: String = "Suspended") {
        self.text = text
        self.monoChromaticLabel(textColor: UIColor().HexToColor(hexString: "#e9a800"), backgroundColor: UIColor().HexToColor(hexString: "#ffeab6"))
    }
    
    func notavailableLabel(text: String = "Not available") {
        self.text = text
        self.monoChromaticLabel(textColor: .gray, backgroundColor: UIColor().HexToColor(hexString: "#dfdfe5"))
    }
}

extension UIButton {
    func monoChromaticButtons(titleColor: UIColor =  .white, backgroundColor: CGColor = UIColor.white.cgColor) {
        self.layer.cornerRadius = self.frame.height/2
        self.layer.backgroundColor = backgroundColor
        self.setTitleColor(titleColor, for: .normal)
        self.layer.masksToBounds = true
    }
    
    func activeButton(title: String = "Activate") {
        self.setTitle(title, for: .normal)
        self.monoChromaticButtons(backgroundColor: UIColor.systemBlue.cgColor)
    }
    
    func subscribeButton(title: String = "Subscribe") {
        self.setTitle(title, for: .normal)
        self.monoChromaticButtons(backgroundColor: UIColor.systemBlue.cgColor)
    }
    
    func deActiveButton(title: String = "Deactivate") {
        self.setTitle(title, for: .normal)
        self.monoChromaticButtons(backgroundColor: UIColor.systemRed.cgColor)
    }
}

enum ImsisType: String {
    case singleimsis
    case multiimsis
}

//Login Response Modal
struct LoginResponseModal: Codable {
    var token: String?
    var user: UserModal?
}

struct UserModal: Codable {
    var status: String?
    var username: String?
    var email: String?
    var permissions: [PermissionsModal]?
}

struct PermissionsModal: Codable {
    var accountId: String?
}

/**Start => Subscribe Sim Card Request Modal**/

struct SubscribeRequestModal: Codable {
    var accountId: String?
    var subscription: SubscriptionRequestModal?
}

struct SubscriptionRequestModal: Codable {
    var subscriberAccountId: String?
    var productId: String?
    var startTime: String?
    var ipPools: [IPPoolsRequestModal]?
}

struct IPPoolsRequestModal: Codable {
    var carrier: String?
    var poolId: String?
}
/**End => Subscribe Sim Card Request Modal**/


/**Start => Product Response Modal**/
struct ProductsListModal: Codable {
    var test : [ProductResponseModal]?
}

struct ProductResponseModal: Codable {
    var _id: String?
    var accountId: String?
    var accountTransferId: String?
    var name: String?
    var networks: ProductNetworksModal?
}

struct ProductNetworksModal: Codable {
    var imsisType: String?
    var singleimsis: String?
    var multiimsis: [String]?
}

/**End => Product Response Modal**/

//Sim Card Details Response Modal
struct SimCardDetailResponseModal: Codable {
    var status: String?
    var type: String?
    var ownerAccountName: String?
    var iccid: String?
    var activationDate: String?
    var suspensionDate: String?
    var reactivationDate: String?
    var subscriptions: [SubscriptionsModal]?
}

struct SubscriptionsModal: Codable {
    var bundles: [BundlesModal]?
    var limit: Int?
    var smsLimit: Int?
}

struct BundlesModal: Codable {
    var dataUsed: Int?
}

struct CommonAPIResponseModal: Codable {
    var status: String?
    var data: String?
}

extension UIButton {
    func enable(isEnable: Bool) {
        self.isEnabled = isEnable
        self.alpha = isEnable ? 1 : 0.6
    }
}

class HomeVC: UIViewController {

    @IBOutlet weak var btnScanBarCode: UIButton!
    @IBOutlet weak var noRecordsFoundView: UIView!
    @IBOutlet weak var lblNoRecords: UILabel!
    @IBOutlet weak var btnMapSimAndGateway: UIButton!
    @IBOutlet weak var btnScanGateWay: UIButton!
    @IBOutlet weak var btnScanTranstek: UIButton!
    
    
    //For Sim card details
    @IBOutlet weak var simcardDetailsBaseView: UIView!
    @IBOutlet weak var lblICCICNumber: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblDataUsed: UILabel!
    @IBOutlet weak var lblDataLimit: UILabel!
    @IBOutlet weak var lblActivationDate: UILabel!
    @IBOutlet weak var lblLastSuspensionDate: UILabel!
    @IBOutlet weak var lblLastReactivationDate: UILabel!
    @IBOutlet weak var btnSimCardAction: UIButton!
    
    
    //For Gateway number
    @IBOutlet weak var lblScannedGatewayNumber: UILabel!
    
    
    private let apiHelper = APIHelper.sharedInstance
    private var activityIndicator = ProgressHUD(text: "")
    
    private var currentSimStatus = ""
    private var currentICCIDNumber = ""
    private var currentGatewayNumber = ""
    private var currentTranstekDeviceSerialNumber = ""
    
    private var currentScanDeviceType: ScanDeviceType = .simCard
    
    
    private var isICCIDNumberVerified: Bool = false {
        didSet {
            if isICCIDNumberVerified && isGatewayVerified {
                self.btnMapSimAndGateway?.enable(isEnable: true)
            } else {
                self.btnMapSimAndGateway?.enable(isEnable: false)
            }
        }
    }
    private var isGatewayVerified: Bool = false {
        didSet {
            if isICCIDNumberVerified && isGatewayVerified {
                self.btnMapSimAndGateway?.enable(isEnable: true)
            } else {
                self.btnMapSimAndGateway?.enable(isEnable: false)
            }
        }
    }
    private var isTranstekVerified: Bool = false {
        didSet {
            if isICCIDNumberVerified && isTranstekVerified {
                self.btnMapSimAndGateway?.enable(isEnable: true)
            } else {
                self.btnMapSimAndGateway?.enable(isEnable: false)
            }
        }
    }
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavigationItems()
        self.initialSetup()
        self.view.addSubview(activityIndicator)
        self.view.backgroundColor = UIColor().HexToColor(hexString: "#F2F8FD")
        
        self.showHideNoDataView(isHidden: false)
        self.resetSimCardInforDetails()
        self.loginServiceCall()
        self.setupNotificationObjects()
        
        //by default disable the mapping button
        self.btnMapSimAndGateway.enable(isEnable: false)
        self.btnSimCardAction?.isHidden = true
    }
    
    override func viewDidLayoutSubviews() {
        self.setupNoRecordsFoundView()
    }
    
    //MARK: - Notifcation Objects
    private func setupNotificationObjects() {
        NotificationCenter.default.addObserver(self, selector: #selector(onGetBarcodeValue), name: .get_barcode_text, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onGetGateWayQRCode), name: .get_gateway_barcode, object: nil)
    }
    
    
    //MARK: - Helper Functions
    private func setupNavigationItems() {
        self.navigationItem.title = "Home"
        self.navigationController?.navigationBar.prefersLargeTitles = true
        //self.navigationItem.searchController = UISearchController()
        self.navigationItem.searchController?.delegate = self
    }
    
    private func initialSetup() {
        self.btnScanBarCode.backgroundColor = .systemBlue
        self.btnScanBarCode.setTitleColor(.white, for: .normal)
        self.btnScanBarCode.layer.cornerRadius = self.btnScanBarCode.frame.height/2
    }
    
    private func setupNoRecordsFoundView() {
        self.lblNoRecords.textColor = .gray
        self.lblNoRecords?.text = "No records found yet!\nClick + icon add the sim card manually"
        self.noRecordsFoundView.addDashedBorder(dashWidth: 1.5, dashColor: .lightGray)
        self.simcardDetailsBaseView.addDashedBorder(dashWidth: 1.5, dashColor: .lightGray)
        self.view.layoutSubviews()
    }
    
    private func showHideNoDataView(isHidden: Bool) {
        self.noRecordsFoundView.isHidden = isHidden
    }
    
    //MARK: - Notification Handler
    @objc private func onGetBarcodeValue(_ notification: NSNotification) {
        print("notification", notification)
        let iccidNumber = notification.userInfo?["potentialQRCode"] as? String ?? ""
        if !iccidNumber.isEmpty {
            let firstTwoChar = String(iccidNumber.prefix(2))
            if firstTwoChar == "89" {
                self.resetSimCardInforDetails() //89011703278617274280
                self.fetchSimCardDetails(iccidNumber: iccidNumber)
                
            } else {
                self.showAlert(message: "Invalid Code")
            }
        }
    }
    
    @objc private func onGetGateWayQRCode(_ notification: NSNotification) {
        
        if let serailNumber = notification.userInfo?["serailNumber"] as? String, !serailNumber.isEmpty {
            self.currentGatewayNumber = serailNumber
            //removw the no record view
            self.showHideNoDataView(isHidden: true)
            self.lblScannedGatewayNumber?.text = serailNumber
            
            self.mappingGatewayAndSimCard(iccidNumber: "", serialNumber: self.currentGatewayNumber, deviceType: .gateway)
        }
    }
    
    //MARK: - Button Actions
    @IBAction func didClickScanButton(_ sender: UIButton) {
        let scannerVC = self.storyboard?.instantiateViewController(withIdentifier: "BarCodeScannerVC") as! BarCodeScannerVC
        //let scannerVC = self.storyboard?.instantiateViewController(withIdentifier: "VisionScannerVC") as! VisionScannerVC
        self.navigationController?.pushViewController(scannerVC, animated: true)
        self.isICCIDNumberVerified = false
    }
    
    @IBAction func didClickScanGatewayButton(_ sender: UIButton) {
        self.currentGatewayNumber = ""
        self.lblScannedGatewayNumber?.text = "-"
        let gateWayScanner = self.storyboard?.instantiateViewController(withIdentifier: "GatewayScannerVC") as! GatewayScannerVC
        gateWayScanner.scanDeviceType       = .gateway
        gateWayScanner.deviceScanDelegate   = self
        
        self.navigationController?.pushViewController(gateWayScanner, animated: true)
        self.isGatewayVerified = false
    }
    
    @IBAction func didClickScanTranstekButton(_ sender: UIButton) {
        
        self.currentTranstekDeviceSerialNumber = ""
        
        let gateWayScanner = self.storyboard?.instantiateViewController(withIdentifier: "GatewayScannerVC") as! GatewayScannerVC
        
        gateWayScanner.scanDeviceType       = .transtek
        gateWayScanner.deviceScanDelegate   = self
        
        self.navigationController?.pushViewController(gateWayScanner, animated: true)
        isTranstekVerified = false
    }
    
    
    @IBAction func didClickMapButton(_ sender: UIButton) {
        //show the alter and get user confirmation before proceed
        
        guard !self.currentICCIDNumber.isEmpty else {
            self.showAlert(message: "Rescan the Simcard")
            return
        }
        
        var deviceSerialNumber = ""
        
        if currentScanDeviceType == .gateway {
            
            guard !self.currentGatewayNumber.isEmpty else {
                self.showAlert(message: "Rescan the Gateway Device")
                return
            }
            
            deviceSerialNumber = currentGatewayNumber
            
        } else if currentScanDeviceType == .transtek {
            
            guard !self.currentTranstekDeviceSerialNumber.isEmpty else {
                self.showAlert(message: "Rescan the Transtek Device")
                return
            }
            
            deviceSerialNumber = currentTranstekDeviceSerialNumber
        }
        
//        guard !self.currentGatewayNumber.isEmpty && !self.currentICCIDNumber.isEmpty else {
//            
//            if self.currentGatewayNumber.isEmpty {
//                self.showAlert(message: "Rescan the Gateway")
//            } else {
//                self.showAlert(message: "Rescan the Simcard")
//            }
//            return
//        }
        
        let alterVC = UIAlertController(title: "Confirmation Alert", message: "\nAre you sure you want map the following \nsimCard \(self.currentICCIDNumber)\nand\nthe \(currentScanDeviceType.description) \(deviceSerialNumber)", preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .default) { okAction in
            //proceed to cal the mapping service call
            self.mappingGatewayAndSimCard(iccidNumber: self.currentICCIDNumber, serialNumber: deviceSerialNumber, deviceType: .gateway)
            
        }
    
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        
        alterVC.addAction(cancelAction)
        alterVC.addAction(okAction)
        
        self.present(alterVC, animated: true, completion: nil)
        
        
    }
    
    @IBAction func didClickSimCardActionButton(_ sender: UIButton) {
        
        guard self.currentSimStatus != SimCardStatus.inactive.rawValue else {
            if !self.currentICCIDNumber.isEmpty {
                self.fetchProductServiceCall(iccidNumber: self.currentICCIDNumber)
            }
            return
        }
        
        var titleString = "Activation Confirmation"
        var message = "\nAre you sure you want to activate the sim card?"
        var action = "unsuspend"
        if self.currentSimStatus == "active" {
            titleString = "Deactivation Confirmation"
            message = "\nAre you sure you want to suspend the sim card?"
            action = "suspend"
        }
        
        let userConfirmation = UIAlertController(title: titleString, message: message , preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        let okAction = UIAlertAction(title: "Ok", style: .default) { okAction in
            
            if !self.currentICCIDNumber.isEmpty {
                self.activeDeactivateServiceCall(iccidNumber: self.currentICCIDNumber, action: action)
            }
        }
        userConfirmation.addAction(cancelAction)
        userConfirmation.addAction(okAction)
        self.present(userConfirmation, animated: true, completion: nil)
    }
    
    @IBAction func didClickAddButton(_ sender: UIBarButtonItem) {
        self.showAlertTextField()
    }
    
    
    private func showAlertTextField() {
        let alertVC = UIAlertController(title: "Enter your ICCID number", message: nil, preferredStyle: .alert)
        alertVC.addTextField()
        let customTextField = alertVC.textFields?[0]
        customTextField?.text = "89"
        customTextField?.keyboardType = .numbersAndPunctuation
        
        let cancelButton = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)

        let submitAction = UIAlertAction(title: "Submit", style: .default) { [unowned alertVC] _ in
            
            if let iccidNumber = customTextField?.text, !iccidNumber.isEmpty, iccidNumber.count >= 18, iccidNumber.count <= 22 {
                let firstTwoChar = String(iccidNumber.prefix(2))
                if firstTwoChar == "89" {
                    self.resetSimCardInforDetails()
                    self.fetchSimCardDetails(iccidNumber: iccidNumber)
                } else {
                    self.showAlert(message: "Invalid Code")
                }
            } else {
                self.showAlert(message: "Invalid Code")
            }
        }

        alertVC.addAction(cancelButton)
        alertVC.addAction(submitAction)
        self.present(alertVC, animated: true)
    }
    
    private func showAlert(message: String) {
        let alertVC = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertVC.addAction(okAction)
        self.present(alertVC, animated: true, completion: nil)
    }
    
    
    //MARK: - Service Call
    private func loginServiceCall() {
        let serviceURL = Constant.baseURL + Constant.login
        let params = [
            "username": Constant.userName,
            "password": Constant.password
        ]
        var data: Data?
        do {
            data = try JSONEncoder().encode(params)
        } catch let encodeError as NSError {
            print("encodeError", encodeError)
        }
        activityIndicator.show(message: "Log In")
        apiHelper.webserviceDataParams(serviceURL: serviceURL, params: data, httpMethod: "POST") { data, error in
            
            DispatchQueue.main.async {
                defer {
                    self.activityIndicator.hide()
                }
                guard error == nil else {
                    return
                }
                
                if let response = data {
                    do {
                        let loginResponseModal = try JSONDecoder().decode(LoginResponseModal.self, from: response)
                        if let token = loginResponseModal.token, !token.isEmpty {
                            UserDefaults.standard.set(token, forKey: "podgrouptoken")
                            UserDefaults.standard.synchronize()
                        }
                        let permissionList = loginResponseModal.user?.permissions?.first
                        if let accountId = permissionList?.accountId, !accountId.isEmpty {
                            UserDefaults.standard.set(accountId, forKey: "accountId")
                            UserDefaults.standard.synchronize()
                        }
                        
                    } catch let decoderError as NSError {
                        print("decoderError", decoderError)
                    }
                }
            }
        }
    }
    
    private func activeDeactivateServiceCall(iccidNumber: String, action: String) {
        let serviceURL = Constant.baseURL + Constant.fetchSimCardDetailsURL + iccidNumber + "/" + action
        var accountID = Constant.accountId
        if let accountId = UserDefaults.standard.value(forKey: "accountId") as? String {
            accountID = accountId
        }
        let params = [
            "accountId": accountID
        ]
        var data: Data?
        do {
            data = try JSONEncoder().encode(params)
        } catch let encodeError as NSError {
            print("encodeError", encodeError)
        }
        activityIndicator.show(message: "Loading")
        apiHelper.webserviceDataParams(serviceURL: serviceURL, params: data, httpMethod: "PUT") { data, error in
            DispatchQueue.main.async {
                defer {
                    self.activityIndicator.hide()
                }
                
                guard error == nil else {
                    return
                }
                
                if let response = data {
                    do {
                        let simCardDetailResponseModal = try JSONDecoder().decode(SimCardDetailResponseModal.self, from: response)
                        print("simCardDetailResponseModal", simCardDetailResponseModal)
                        
                        self.resetSimCardInforDetails()
                        self.fetchSimCardDetails(iccidNumber: iccidNumber)
                    } catch let decoderError as NSError {
                        print("decoderError", decoderError)
                    }
                }
            }
        }
    }
    
    private func fetchProductServiceCall(iccidNumber: String) {
        let serviceURL = Constant.baseURL + Constant.fetchProductsURL
        var accountID = Constant.accountId
        if let accountId = UserDefaults.standard.value(forKey: "accountId") as? String {
            accountID = accountId
        }
        let params = [
            "accountId": accountID,
            "iccid": iccidNumber
        ]
        activityIndicator.show(message: "Getting Product Details")
        apiHelper.webserviceQueryParamsMethod(serviceURL: serviceURL, params: params, httpMethod: "GET") { data, error in
            
            DispatchQueue.main.async {
                defer {
                    self.activityIndicator.hide()
                }
                
                guard error == nil else {
                    self.showHideNoDataView(isHidden: false)
                    return
                }
                
                if let response = data {
                    do {
                        let productLists = try JSONSerialization.jsonObject(with: response, options: .allowFragments) as! [[String: Any]]
                        self.processProductDetails(iccidNumber: iccidNumber, productLists: productLists)
                    } catch let decoderError as NSError {
                        print("decoderError", decoderError)
                        //self.showHideNoDataView(isHidden: false)
                    }
                } else {
                    //self.showHideNoDataView(isHidden: false)
                }
            }
        }
    }
    
    private func processProductDetails(iccidNumber: String, productLists: [[String: Any]]) {
        guard !productLists.isEmpty else {
            self.showAlert(message: "No Active Products founds for this iccid number")
            return
        }
        
        var showMultiProductAlert = true
        var productNames = [String]()
        
        for productList in productLists {
            let planName = productList["name"] as? String ?? ""
            productNames.append(planName)
            if planName == Constant.defaultPlanName || planName == Constant.defaultPlanNameTwo {
                self.showSubscribeConfirmAlert(iccidNumber: iccidNumber, productDetails: productList)
                showMultiProductAlert = false
                break
            }
        }
        
        if showMultiProductAlert {
            let names = productNames.map({$0}).joined(separator: ",\n")
            self.showAlert(message: "Multiple products found\n\(names)")
        }
    }
    
    private func showSubscribeConfirmAlert(iccidNumber: String, productDetails: [String: Any]) {
        let planName = productDetails["name"] as? String ?? ""
        let productId = productDetails["_id"] as? String ?? ""
        let networks = productDetails["networks"] as? [String: Any] ?? [:]
        
        let imsisType = networks["imsisType"] as? String ?? ""
        let carrier = networks["singleimsis"] as? String ?? ""
        
        guard !productId.isEmpty else {
            self.showAlert(message: "Unable to subscribe the sim card")
            return
        }
        
        guard !carrier.isEmpty else {
            self.showAlert(message: "Unable to subscribe the sim card")
            return
        }
        
        
        guard imsisType == ImsisType.singleimsis.rawValue else {
            self.showAlert(message: "ImsisType mismatch")
            return
        }
        
        //allow only for the single imsis type for now
        let confirmAlert = UIAlertController(title: "Confirmation Alert", message: "\nAre your sure you want to activate the sim card with \(planName)", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Activate", style: .default) { alert in
            self.subscribeServiceCall(iccidNumber: iccidNumber, productId: productId, carrier: carrier)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive) { cancel in
            print("Click Cancel")
        }
        
        confirmAlert.addAction(cancelAction)
        confirmAlert.addAction(okAction)
        self.present(confirmAlert, animated: true, completion: nil)
    }
    
    
    //activate or subscribe the new sim card
    private func subscribeServiceCall(iccidNumber: String, productId: String, carrier: String) {
        let serviceURL = Constant.baseURL + Constant.fetchSimCardDetailsURL + iccidNumber + Constant.subscribe
        var accountID = Constant.accountId
        if let accountId = UserDefaults.standard.value(forKey: "accountId") as? String {
            accountID = accountId
        }
        
        let ipPoolRequestModal = IPPoolsRequestModal(carrier: carrier, poolId: "")
        let subscriptionRequestModal = SubscriptionRequestModal(subscriberAccountId: accountID, productId: productId, startTime: "", ipPools: [ipPoolRequestModal])
        let requestModal = SubscribeRequestModal(accountId: accountID, subscription: subscriptionRequestModal)
        var data: Data?
        do {
            data = try JSONEncoder().encode(requestModal)
        } catch let encodeError as NSError {
            print("encodeError", encodeError)
        }
        activityIndicator.show(message: "Subscribing")
        apiHelper.webserviceDataParams(serviceURL: serviceURL, params: data, httpMethod: "PUT") { data, error in
            DispatchQueue.main.async {
                defer {
                    self.activityIndicator.hide()
                }
                
                guard error == nil else {
                    print("error", error)
                    return
                }
                
                if let response = data {
                    do {
                        let simCardDetailResponseModal = try JSONDecoder().decode(SimCardDetailResponseModal.self, from: response)
                        print("simCardDetailResponseModal", simCardDetailResponseModal)
                        
                        self.resetSimCardInforDetails()
                        self.fetchSimCardDetails(iccidNumber: iccidNumber)
                    } catch let decoderError as NSError {
                        print("decoderError", decoderError)
                    }
                }
            }
        }
    }
    
    private func fetchSimCardDetails(iccidNumber: String) {
        let serviceURL = Constant.baseURL + Constant.fetchSimCardDetailsURL + iccidNumber
        var accountID = Constant.accountId
        if let accountId = UserDefaults.standard.value(forKey: "accountId") as? String {
            accountID = accountId
        }
        let params = [
            "accountId": accountID
        ]
        activityIndicator.show(message: "Getting Simcard Details")
        apiHelper.webserviceQueryParamsMethod(serviceURL: serviceURL, params: params, httpMethod: "GET") { data, error in
            DispatchQueue.main.async {
                defer {
                    self.activityIndicator.hide()
                }
                guard error == nil else {
                    self.showHideNoDataView(isHidden: false)
                    return
                }
                
                if let response = data {
                    do {
                        let simCardDetailResponseModal = try JSONDecoder().decode(SimCardDetailResponseModal.self, from: response)
                        print("simCardDetailResponseModal", simCardDetailResponseModal)
                        self.showSimCardDetails(simCardDetailResponseModal: simCardDetailResponseModal)
                        self.btnSimCardAction?.isHidden = false
                        self.showHideNoDataView(isHidden: true)
                    } catch let decoderError as NSError {
                        print("decoderError", decoderError)
                        self.showHideNoDataView(isHidden: false)
                    }
                } else {
                    self.showHideNoDataView(isHidden: false)
                }
            }
        }
    }
    
    private func resetSimCardInforDetails() {
        self.lblICCICNumber?.text = ""
        self.lblStatus?.text = ""
        self.lblActivationDate?.text = ""
        self.lblLastSuspensionDate?.text = ""
        self.lblLastReactivationDate?.text = ""
        self.lblDataLimit?.text = ""
        self.lblDataUsed?.text = ""
        self.currentICCIDNumber = ""
        self.currentSimStatus = ""
    }
    
    private func showSimCardDetails(simCardDetailResponseModal: SimCardDetailResponseModal?) {
        if let simCardInfo = simCardDetailResponseModal {
            
            if let iccid = simCardInfo.iccid, !iccid.isEmpty {
                self.lblICCICNumber?.text = "ICCID: " + iccid
                
                self.currentICCIDNumber = iccid
            }
            
            let simStatus = simCardInfo.status
            if simStatus == SimCardStatus.active.rawValue {
                self.lblStatus?.activeLabel()
                self.btnSimCardAction?.deActiveButton()
                
                self.currentSimStatus = simStatus ?? ""
                
            } else if simStatus == SimCardStatus.suspended.rawValue {
                self.lblStatus?.suspendedLabel()
                self.btnSimCardAction?.activeButton()
                
                self.currentSimStatus = simStatus ?? ""
                
            } else if simStatus == SimCardStatus.inactive.rawValue {
                self.lblStatus?.notavailableLabel(text: "Inactive")
                self.btnSimCardAction?.subscribeButton()
                
                self.currentSimStatus = simStatus ?? ""
                
            } else {
                self.lblStatus?.notavailableLabel()
                self.btnSimCardAction?.activeButton()
            }
            
            if let activationDate = simCardInfo.activationDate, !activationDate.isEmpty {
                self.lblActivationDate?.text = self.dateConversion(dateString: activationDate)
            }
            
            if let suspensionDate = simCardInfo.suspensionDate, !suspensionDate.isEmpty {
                self.lblLastSuspensionDate?.text = self.dateConversion(dateString: suspensionDate)
            }
            
            if let reactivationDate = simCardInfo.reactivationDate, !reactivationDate.isEmpty {
                self.lblLastReactivationDate?.text = self.dateConversion(dateString: reactivationDate)
            }
            
            let subscriptions = simCardInfo.subscriptions?.first
            if let limit = subscriptions?.limit {
                self.lblDataLimit?.text = self.convertBytesToMB(bytes: limit)
            }
            
            let bundle = subscriptions?.bundles?.first
            if let dataUsed = bundle?.dataUsed {
                self.lblDataUsed?.text = self.convertBytesToMB(bytes: dataUsed)
            }
            
            if !self.currentICCIDNumber.isEmpty {
                self.mappingGatewayAndSimCard(iccidNumber: self.currentICCIDNumber, serialNumber: "", deviceType: currentScanDeviceType)
            }
        }
    }
    
    private func convertBytesToMB(bytes: Int) -> String {
        let bytesInDouble: Double = Double(bytes)
        let convertedMB = bytesInDouble/1024/1024
        return String(format: "%.2f MB", convertedMB)
    }
    
    private func dateConversion(dateString: String) -> String {
        let dateFormatter = DateFormatter()
        let tempLocale = dateFormatter.locale // save locale temporarily
        dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let date = dateFormatter.date(from: dateString)!
        dateFormatter.dateFormat = "MM-dd-yyyy HH:mm:ss"
        dateFormatter.locale = tempLocale // reset the locale
        let dateString = dateFormatter.string(from: date)
        return dateString
    }
    
}

extension HomeVC {
    //MARK: - For Gateway Service Call
    private func mappingGatewayAndSimCard(iccidNumber: String, serialNumber: String, deviceType: ScanDeviceType) {
        
        let serviceURL = Constant.mapSimCardURL
        let params = [
            "iccidNumber": iccidNumber,
            "serialNumber": serialNumber
        ]
        
        activityIndicator.show(message: "Checking")
        apiHelper.webserviceQueryParamsMethod(serviceURL: serviceURL, params: params, httpMethod: "POST") { data, error in
            DispatchQueue.main.async {
                defer {
                    self.activityIndicator.hide()
                }
                
                guard error == nil else {
                    self.showAlert(message: "Please try again later!")
                    return
                }
                
                if let data = data {
                    
                    do {
                        let responseModal = try JSONDecoder().decode(CommonAPIResponseModal.self, from: data)
                        print("responseModal", responseModal)
//                        guard let status = responseModal.status, status == "success" else {
//                            if let message = responseModal.data, !message.isEmpty {
//                                self.showAlert(message: message)
//                            }
//                            return
//                        }
//                        //status success, allow to mapping the sim card or gateway
//                        if let message = responseModal.data, !message.isEmpty {
//                            self.showAlert(message: message)
//                        }
                        self.updateMappingButtonStatus(params: params, responseModal: responseModal, deviceType: deviceType)
                    } catch let error as NSError {
                        print("Error", error.localizedDescription)
                        self.showAlert(message: error.localizedDescription)
                    }
                } else {
                    self.showAlert(message: "Unknow error. Please try again later!")
                }
            }
        }
    }
    
    private func updateMappingButtonStatus(params: [String: String], responseModal: CommonAPIResponseModal, deviceType: ScanDeviceType) {
        
        let simcardNumber = params["iccidNumber"] ?? ""
        let gatewayNumber = params["serialNumber"] ?? ""
        let status = responseModal.status ?? ""
        let message = responseModal.data ?? ""
        
        if status == "success" {
            if !simcardNumber.isEmpty && !gatewayNumber.isEmpty {
                self.showAlert(message: message)
                self.isGatewayVerified = false
                self.isTranstekVerified = false
                self.isICCIDNumberVerified = false
                
                //clear the value
//                self.currentICCIDNumber = ""
                self.currentGatewayNumber = ""
                self.currentTranstekDeviceSerialNumber = ""
                self.lblScannedGatewayNumber?.text = "-"
                
            } else if !simcardNumber.isEmpty {
                self.isICCIDNumberVerified = true
                
            } else if !gatewayNumber.isEmpty {
                //show the scanned gate way and success message
                if deviceType == .gateway {
                    self.isGatewayVerified = true
                } else if deviceType == .transtek {
                    self.isTranstekVerified = true
                }
                
                let formattedMsg = "Scanned \(deviceType.description): " + gatewayNumber
                self.showAlert(message: formattedMsg)
            }
        } else {
            //failure scenario
            if !simcardNumber.isEmpty && !gatewayNumber.isEmpty {
                self.showAlert(message: message)
                
            } else if !simcardNumber.isEmpty {
                self.isICCIDNumberVerified = false
                self.showAlert(message: message)
                
            } else if !gatewayNumber.isEmpty {
                let formattedMsg = "Scanned \(deviceType.description): " + gatewayNumber + "\n\n" + message
                self.showAlert(message: formattedMsg)
                self.isGatewayVerified = false
                self.isTranstekVerified = false
            }
        }
    }
}


extension HomeVC: UISearchControllerDelegate {
    func willDismissSearchController(_ searchController: UISearchController) {
        self.view.layoutSubviews()
        self.viewDidLayoutSubviews()
    }
}


extension HomeVC: DeviceScanProtocol {
    
    func didScanDevice(scanDeviceType: ScanDeviceType, serialNumber: String?) {
        
        btnScanTranstek.enable(isEnable: true)
        btnScanGateWay.enable(isEnable: true)
        
        currentTranstekDeviceSerialNumber = ""
        
        if scanDeviceType == .gateway {
            
            if let serailNumber = serialNumber, !serailNumber.isEmpty {
                self.currentGatewayNumber = serailNumber
                //removw the no record view
                self.showHideNoDataView(isHidden: true)
                self.lblScannedGatewayNumber?.text = serailNumber
                
                currentScanDeviceType = .gateway
                
                self.mappingGatewayAndSimCard(iccidNumber: "", serialNumber: self.currentGatewayNumber, deviceType: .gateway)
                
                btnScanTranstek.enable(isEnable: false)
                
                btnMapSimAndGateway.setTitle("Map Simcard & Gateway", for: .normal)
            }
            
        } else if scanDeviceType == .transtek {
            
            if let serailNumber = serialNumber, !serailNumber.isEmpty {
                //removw the no record view
                self.showHideNoDataView(isHidden: true)
                self.lblScannedGatewayNumber?.text = serailNumber
                
                currentScanDeviceType = .transtek
                
                currentTranstekDeviceSerialNumber = serailNumber
                
                self.mappingGatewayAndSimCard(iccidNumber: "", serialNumber: serailNumber, deviceType: .transtek)
                
                btnScanGateWay.enable(isEnable: false)
                
                btnMapSimAndGateway.setTitle("Map Simcard & Transtek", for: .normal)
            }
        }
    }
}


class ProgressHUD: UIVisualEffectView {
    
    var text: String? {
      didSet {
        label.text = text
      }
    }

    let activityIndictor: UIActivityIndicatorView = UIActivityIndicatorView(style: .large)
    let label: UILabel = UILabel()
    let blurEffect = UIBlurEffect(style: .dark)
    let vibrancyView: UIVisualEffectView

    init(text: String) {
      self.text = text
      self.vibrancyView = UIVisualEffectView(effect: UIVibrancyEffect(blurEffect: blurEffect))
      super.init(effect: blurEffect)
      self.setup()
    }

    required init?(coder aDecoder: NSCoder) {
      self.text = ""
      self.vibrancyView = UIVisualEffectView(effect: UIVibrancyEffect(blurEffect: blurEffect))
      super.init(coder: aDecoder)
      self.setup()
    }

    func setup() {
      contentView.addSubview(vibrancyView)
      contentView.addSubview(activityIndictor)
      contentView.addSubview(label)
      activityIndictor.startAnimating()
    }

    override func didMoveToSuperview() {
      super.didMoveToSuperview()

      if let superview = self.superview {

        let width = superview.frame.size.width / 2.3
        let height: CGFloat = 50.0
        self.frame = CGRect(x: superview.frame.size.width / 2 - width / 2,
                        y: superview.frame.height / 2 - height / 2,
                        width: width,
                        height: height)
        vibrancyView.frame = self.bounds

        let activityIndicatorSize: CGFloat = 40
        activityIndictor.frame = CGRect(x: 5,
                                        y: height / 2 - activityIndicatorSize / 2,
                                        width: activityIndicatorSize,
                                        height: activityIndicatorSize)

        layer.cornerRadius = 8.0
        layer.masksToBounds = true
        label.text = text
        label.textAlignment = NSTextAlignment.center
        label.frame = CGRect(x: activityIndicatorSize + 5,
                             y: 0,
                             width: width - activityIndicatorSize - 15,
                             height: height)
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 16)
      }
    }

    func show(message: String) {
      label.text = message
      self.isHidden = false
    }

    func hide() {
      self.isHidden = true
    }
}
