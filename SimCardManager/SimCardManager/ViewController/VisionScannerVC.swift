//
//  VisionScannerVC.swift
//  SimCardManager
//
//  Created by Humworld Solutions Private Limited on 26/10/21.
//

import UIKit
import AVFoundation
import Vision
//import Anchors
import TesseractOCR

extension UIViewController {
  func add(childController: UIViewController) {
      childController.willMove(toParent: self)
    view.addSubview(childController.view)
      childController.didMove(toParent: self)
  }
}

class VisionScannerVC: UIViewController {

    private let cameraController = CameraController()
    private let visionService = VisionService()
    private let boxService = BoxService()
    private let ocrService = OCRService()

    private lazy var label: UILabel = {
      let label = UILabel()
      label.textAlignment = .right
      label.font = UIFont.preferredFont(forTextStyle: .headline)
      label.textColor = .black
      return label
    }()

    override func viewDidLoad() {
      super.viewDidLoad()

      cameraController.delegate = self
      add(childController: cameraController)
//      activate(
//        cameraController.view.anchor.edges
//      )

      view.addSubview(label)
      //activate(label.anchor.bottom.right.constant(-20))

      visionService.delegate = self
      boxService.delegate = self
      ocrService.delegate = self
    }

    override func viewDidAppear(_ animated: Bool) {
      super.viewDidAppear(animated)

     // musicService.play(fileName: "introduction")
    }
    
}

extension VisionScannerVC: CameraControllerDelegate {
  func cameraController(_ controller: CameraController, didCapture buffer: CMSampleBuffer) {
    visionService.handle(buffer: buffer)
  }
}

extension VisionScannerVC: VisionServiceDelegate {
  func visionService(_ version: VisionService, didDetect image: UIImage, results: [VNTextObservation]) {
    boxService.handle(
      cameraLayer: cameraController.cameraLayer,
      image: image,
      results: results,
      on: cameraController.view
    )
  }
}

extension VisionScannerVC: BoxServiceDelegate {
  func boxService(_ service: BoxService, didDetect images: [UIImage]) {
    guard let biggestImage = images.sorted(by: {
      $0.size.width > $1.size.width && $0.size.height > $1.size.height
    }).first else {
      return
    }

    ocrService.handle(image: biggestImage)
  }
}

extension VisionScannerVC: OCRServiceDelegate {
  func ocrService(_ service: OCRService, didDetect text: String) {
    label.text = text
      print("label text", text)
   // musicService.handle(text: text)
  }
}


//MARK: - Camera
protocol CameraControllerDelegate: class {
  func cameraController(_ controller: CameraController, didCapture buffer: CMSampleBuffer)
}

final class CameraController: UIViewController, AVCaptureVideoDataOutputSampleBufferDelegate {
  private(set) lazy var cameraLayer: AVCaptureVideoPreviewLayer = AVCaptureVideoPreviewLayer(session: self.captureSession)

  private lazy var captureSession: AVCaptureSession = {
    let session = AVCaptureSession()
    session.sessionPreset = AVCaptureSession.Preset.photo

    guard
      let backCamera = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .back),
      let input = try? AVCaptureDeviceInput(device: backCamera)
    else {
      return session
    }

    session.addInput(input)
    return session
  }()

  weak var delegate: CameraControllerDelegate?

  override func viewDidLoad() {
    super.viewDidLoad()

    cameraLayer.videoGravity = .resizeAspectFill
    view.layer.addSublayer(cameraLayer)

    // register to receive buffers from the camera
    let videoOutput = AVCaptureVideoDataOutput()
    videoOutput.setSampleBufferDelegate(self, queue: DispatchQueue(label: "MyQueue"))
    self.captureSession.addOutput(videoOutput)

    // begin the session
    self.captureSession.startRunning()
  }

  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()

    // make sure the layer is the correct size
    self.cameraLayer.frame = view.bounds
  }

  func captureOutput(
    _ output: AVCaptureOutput,
    didOutput sampleBuffer: CMSampleBuffer,
    from connection: AVCaptureConnection) {

    sample = sampleBuffer
  }

  // FIXME: Test

  var sample: CMSampleBuffer?

  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    super.touchesBegan(touches, with: event)

    if let sample = sample {
      delegate?.cameraController(self, didCapture: sample)
    }
  }
}


//MARK: - Vision
protocol VisionServiceDelegate: class {
  func visionService(_ version: VisionService, didDetect image: UIImage, results: [VNTextObservation])
}

final class VisionService {

  weak var delegate: VisionServiceDelegate?

  func handle(buffer: CMSampleBuffer) {
    guard let pixelBuffer = CMSampleBufferGetImageBuffer(buffer) else {
      return
    }

    let ciImage = CIImage(cvPixelBuffer: pixelBuffer)
    guard let image = ciImage.toUIImage() else {
      return
    }

    makeRequest(image: image)
  }

  private func inferOrientation(image: UIImage) -> CGImagePropertyOrientation {
    switch image.imageOrientation {
    case .up:
      return CGImagePropertyOrientation.up
    case .upMirrored:
      return CGImagePropertyOrientation.upMirrored
    case .down:
      return CGImagePropertyOrientation.down
    case .downMirrored:
      return CGImagePropertyOrientation.downMirrored
    case .left:
      return CGImagePropertyOrientation.left
    case .leftMirrored:
      return CGImagePropertyOrientation.leftMirrored
    case .right:
      return CGImagePropertyOrientation.right
    case .rightMirrored:
      return CGImagePropertyOrientation.rightMirrored
    }
  }

  private func makeRequest(image: UIImage) {
    guard let cgImage = image.cgImage else {
      assertionFailure()
      return
    }

    let handler = VNImageRequestHandler(
      cgImage: cgImage,
      orientation: inferOrientation(image: image),
      options: [VNImageOption: Any]()
    )

    let request = VNDetectTextRectanglesRequest(completionHandler: { [weak self] request, error in
      DispatchQueue.main.async {
        self?.handle(image: image, request: request, error: error)
      }
    })

    request.reportCharacterBoxes = true

    do {
      try handler.perform([request])
    } catch {
      print(error as Any)
    }
  }

  private func handle(image: UIImage, request: VNRequest, error: Error?) {
    guard
      let results = request.results as? [VNTextObservation]
    else {
      return
    }

    delegate?.visionService(self, didDetect: image, results: results)
  }
}

//MARK: - Box Service
protocol BoxServiceDelegate: class {
  func boxService(_ service: BoxService, didDetect images: [UIImage])
}

final class BoxService {
  private var layers: [CALayer] = []

  weak var delegate: BoxServiceDelegate?

  func handle(cameraLayer: AVCaptureVideoPreviewLayer, image: UIImage, results: [VNTextObservation], on view: UIView) {
    reset()

    var images: [UIImage] = []
    let results = results.filter({ $0.confidence > 0.5 })

    layers = results.map({ result in
      let layer = CALayer()
      view.layer.addSublayer(layer)
      layer.borderWidth = 2
      layer.borderColor = UIColor.green.cgColor

      do {
        var transform = CGAffineTransform.identity
        transform = transform.scaledBy(x: image.size.width, y: -image.size.height)
        transform = transform.translatedBy(x: 0, y: -1)
        let rect = result.boundingBox.applying(transform)

        let scaleUp: CGFloat = 0.2
        let biggerRect = rect.insetBy(
          dx: -rect.size.width * scaleUp,
          dy: -rect.size.height * scaleUp
        )

        if let croppedImage = crop(image: image, rect: biggerRect) {
          images.append(croppedImage)
        }
      }

      do {
        let rect = cameraLayer.layerRectConverted(fromMetadataOutputRect: result.boundingBox)
        layer.frame = rect
      }

      return layer
    })

    delegate?.boxService(self, didDetect: images)
  }

  private func crop(image: UIImage, rect: CGRect) -> UIImage? {
    guard let cropped = image.cgImage?.cropping(to: rect) else {
      return nil
    }

    return UIImage(cgImage: cropped, scale: image.scale, orientation: image.imageOrientation)
  }

  private func reset() {
    layers.forEach {
      $0.removeFromSuperlayer()
    }

    layers.removeAll()
  }
}

//MARK: - OCR Service
protocol OCRServiceDelegate: class {
  func ocrService(_ service: OCRService, didDetect text: String)
}

final class OCRService {
  //private let instance = SwiftOCR()
  private let tesseract = G8Tesseract(language: "eng")!

  weak var delegate: OCRServiceDelegate?

  init() {
    tesseract.engineMode = .tesseractCubeCombined
    tesseract.pageSegmentationMode = .singleBlock
  }

  func handle(image: UIImage) {
    handleWithTesseract(image: image)
  }

//  private func handleWithSwiftOCR(image: UIImage) {
//    instance.recognize(image, { string in
//      DispatchQueue.main.async {
//        self.delegate?.ocrService(self, didDetect: string)
//      }
//    })
//  }

  private func handleWithTesseract(image: UIImage) {
    tesseract.image = image.g8_blackAndWhite()
    tesseract.recognize()
    let text = tesseract.recognizedText ?? ""
    delegate?.ocrService(self, didDetect: text)
  }
}
