//
//  GatewayScannerVC.swift
//  SimCardManager
//
//  Created by Humworld Solutions Private Limited on 16/02/22.
//

import UIKit
import AVFoundation

enum ScanDeviceType {
    case gateway
    case transtek
    case simCard
    
    var description: String {
        switch self {
        case .gateway:
            return "Gateway"
        case .transtek:
            return "Transtek"
        case .simCard:
            return "Sim Card"
        }
    }
}

extension String {
    var isNumber: Bool {
        let digitsCharacters = CharacterSet(charactersIn: "0123456789")
        return CharacterSet(charactersIn: self).isSubset(of: digitsCharacters)
    }
}

protocol DeviceScanProtocol {
    func didScanDevice(scanDeviceType: ScanDeviceType, serialNumber: String?)
}


class GatewayScannerVC: UIViewController {

    //MARK: - Outlets & Properties
    private var avCaptureSession = AVCaptureSession()
    private var avPreviewLayer: AVCaptureVideoPreviewLayer?
    private var qrCodeFrameView: UIView?
    
    public var scanDeviceType: ScanDeviceType = .gateway
    public var deviceScanDelegate: DeviceScanProtocol?
    
    
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Scan Gateway QR Code"
        setupVideoInputOutputDevice()
        //setupQRDetectedView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //need to stop running capture session
        if (avCaptureSession.isRunning) {
            avCaptureSession.stopRunning()
        }
    }
    
    override func viewDidLayoutSubviews() {
        setupPreviewLayerFrame()
    }
    
    //MARK: - Logical Functions
    private func setupPreviewLayerFrame() {
        let bounds = view.layer.bounds
        avPreviewLayer?.position = CGPoint(x: bounds.midX, y: bounds.midY)
    }
    
    private func setupVideoInputOutputDevice() {
        
        //get the devices with camera using discovery session

        let discoverySession = AVCaptureDevice.default(for: .video)
        
        //add the capture input device to the capture session
        do {
            let inputDevice = try AVCaptureDeviceInput(device: discoverySession!)
            avCaptureSession.addInput(inputDevice)
        } catch {
            print("device can not be added")
            return
        }
        
        //add the output to the capture session
        let captureMediaOutput = AVCaptureMetadataOutput()
        avCaptureSession.addOutput(captureMediaOutput)
        
        captureMediaOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        
        if scanDeviceType == .gateway {
            captureMediaOutput.metadataObjectTypes = [.qr, .ean8, .ean13, .pdf417]
        } else if scanDeviceType == .transtek {
            captureMediaOutput.metadataObjectTypes = [.code128]
        }
        
        
        //add the preview layer to the app
        avPreviewLayer = AVCaptureVideoPreviewLayer(session: avCaptureSession)
        avPreviewLayer?.videoGravity = .resizeAspectFill
        avPreviewLayer?.frame = self.view.layer.bounds
        self.view.layer.addSublayer(avPreviewLayer!)
        
        setupPreviewLayerFrame()
        
        DispatchQueue.global(qos: .background).async {
            self.avCaptureSession.startRunning()
        }
    }
    
    
    //MARK:- Button Actions
    @IBAction func didClickRescanButton(_ sender: UIButton) {
        if !avCaptureSession.isRunning {
            avCaptureSession.startRunning()
        }
    }

}

extension GatewayScannerVC: AVCaptureMetadataOutputObjectsDelegate {
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        
        if (metadataObjects.count == 0) {
            //No objects found so far
            print("NO QR Objects found so far")
            //qrCodeFrameView?.frame = .zero
            return
        }
        
        if let metaDataObject = metadataObjects.first {
            guard let readableObject = metaDataObject as? AVMetadataMachineReadableCodeObject else {
                return
            }
            print("readableObject type", readableObject.type)
            defer {
                setupPreviewLayerFrame()
            }
            if readableObject.type == AVMetadataObject.ObjectType.qr || readableObject.type == AVMetadataObject.ObjectType.code128 {
                
                let barCodeObject = avPreviewLayer?.transformedMetadataObject(for: readableObject)
                self.view.frame = barCodeObject!.bounds
                
                guard let stringValue = readableObject.stringValue else {
                    return
                }
                avCaptureSession.stopRunning()
                AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
                print("found QR Code", stringValue)
                
                if scanDeviceType == .transtek {
                    
                    self.processTranstekQRCodes(qrCode: stringValue)
                    
                } else if scanDeviceType == .gateway {
                    
                    self.processGatewayQRCode(qrCode: stringValue)
                }
            } else {
                avCaptureSession.stopRunning()
                self.handleInvalidQRCode(isInvalid: true)
            }
        }
    }
    
    private func processTranstekQRCodes(qrCode: String) {
        
        var isValidQRCodes = false
        
        defer {
            if isValidQRCodes {
                DispatchQueue.main.async {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
        
        guard !qrCode.isEmpty && qrCode.count == 15 else {
            self.handleInvalidQRCode(isInvalid: true)
            return
        }
        
        //check must be only numeric value
        guard qrCode.isNumber else { return }
        
        deviceScanDelegate?.didScanDevice(scanDeviceType: .transtek, serialNumber: qrCode)
        //NotificationCenter.default.post(name: .get_gateway_barcode, object: nil, userInfo: ["serailNumber": qrCode])
        self.navigationController?.popViewController(animated: true)
    }
    
    private func processGatewayQRCode(qrCode: String) {
        
        guard qrCode != "" && (qrCode.count == 22 || qrCode.count == 17 || qrCode.count == 12) else {
            self.handleInvalidQRCode(isInvalid: true)
            return
        }
        
        var rawSerailNumber: String = qrCode
        if qrCode.count == 22 {
            let qrCodeArray = qrCode.split(separator: "|")
            rawSerailNumber = String(qrCodeArray[0])
        }
        
        let serailNumber = rawSerailNumber.replacingOccurrences(of: ":", with: "")
        //do the service call for validation and save
        guard serailNumber.count == 12 else {
            self.handleInvalidQRCode(isInvalid: true)
            return
        }
        
        deviceScanDelegate?.didScanDevice(scanDeviceType: .gateway, serialNumber: serailNumber)
        //NotificationCenter.default.post(name: .get_gateway_barcode, object: nil, userInfo: ["serailNumber": serailNumber])
        self.navigationController?.popViewController(animated: true)
    }
    
    private func handleInvalidQRCode(isInvalid: Bool) {
        var message = "Invalid QR Code"
        if isInvalid == false {
            message = "Timeout"
        }
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        let reScanAction = UIAlertAction(title: "Rescan", style: .default) { (rescan) in
            self.setupPreviewLayerFrame()
            self.avCaptureSession.startRunning()
        }
        let cancel = UIAlertAction(title: "Cancel", style: .destructive) { (cancel) in
            self.navigationController?.popViewController(animated: true)
        }
        alertController.addAction(cancel)
        alertController.addAction(reScanAction)
        self.present(alertController, animated: true, completion: nil)
    }
}
