//
//  BPHomeVC.swift
//  SimCardManager
//
//  Created by Humworld Solutions Private Limited on 19/12/22.
//

import UIKit
import CoreBluetooth

extension Data {
    struct HexEncodingOptions: OptionSet {
        let rawValue: Int
        static let upperCase = HexEncodingOptions(rawValue: 1 << 0)
    }

    func hexEncodedString(options: HexEncodingOptions = []) -> String {
        let format = options.contains(.upperCase) ? "%02hhX" : "%02hhx"
        return map { String(format: format, $0) }.joined()
    }
    
    func hexEncodedFlipedString(options: HexEncodingOptions = []) -> String {
        let format = options.contains(.upperCase) ? "%02hhX" : "%02hhx"
        return map { String(format: format, $0) }.reversed().joined()
    }
}

extension UIView {
    func cornerRadius(radius: CGFloat) {
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
    }
}

extension UIImage {

    /// Get image from given view
    ///
    /// - Parameter view: the view
    /// - Returns: UIImage
    public class func image(fromView view: UIView) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(view.frame.size, false, 0)
        view.drawHierarchy(in: view.bounds, afterScreenUpdates: false)

        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
}

class BPHomeVC: UIViewController, CBCentralManagerDelegate {
    
        
    @IBOutlet weak var baseView: UIView!
    
    
    private var cbManager: CBCentralManager?
    
    private var urionMACAddress: String?
    private var printView: UIView? = nil
    
    private var activityIndicator = ProgressHUD(text: "")
    private var isIndicatorAlreadyAdded: Bool = false
    private var scanTimer: Timer?
    
    private var scanButton = UIButton()
    
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        layoutSetupForBPScan()
    }
    


    private func layoutSetupForBPScan() {
        
        //remove all the previously added subview if any
        let _ = baseView.subviews.map({$0.removeFromSuperview()})
        
        printView = nil
        
        let container: UIView = self.baseView
        
        //create a button for scan BP deivce
        scanButton = getButton(title: "Scan BP device")
        scanButton.addTarget(self, action: #selector(didClickScanButton), for: .touchUpInside)
        
        scanButton.translatesAutoresizingMaskIntoConstraints = false
        container.addSubview(scanButton)
        
        NSLayoutConstraint.activate([
            scanButton.widthAnchor.constraint(equalToConstant: 200),
            scanButton.heightAnchor.constraint(equalToConstant: 40),
            scanButton.centerYAnchor.constraint(equalTo: container.centerYAnchor, constant: 70),
            scanButton.centerXAnchor.constraint(equalTo: container.centerXAnchor, constant: 0)
        ])
        
        scanButton.cornerRadius(radius: 20)
    }
    
    
    
    private func showAlert(message: String) {
        let alertVC = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertVC.addAction(okAction)
        self.present(alertVC, animated: true, completion: nil)
    }
    
    
    //MARK: - Layout Helper
    private func getButton(title: String) -> UIButton {
        let scanButton = UIButton()
        scanButton.setTitle(title, for: .normal)
        scanButton.backgroundColor = .systemBlue
        scanButton.titleLabel?.font = .systemFont(ofSize: 16)
        
        return scanButton
    }
    
    private func getLabel(valueString: String) -> UILabel {
        let label = UILabel()
        label.text = valueString
        label.textAlignment = .center
        label.textColor = .black
        label.font = .systemFont(ofSize: 16)
        
        return label
    }
    
    private func showActivityIndicator(message: String, show: Bool) {
        
        if show {
            
            if !isIndicatorAlreadyAdded {
                self.view.addSubview(activityIndicator)
                isIndicatorAlreadyAdded = true
            }
            
            activityIndicator.show(message: message)
            
        } else {
            activityIndicator.hide()
        }
    }

    //MARK: - Button Actions
    @objc private func didClickScanButton(_ sender: UIButton) {
        
        if let _ = cbManager {
            
            self.startBLEScan()
  
        } else {

            initiateBLE()
        }
        
        scanButton.enable(isEnable: false)
    }
    
    @objc private func didClickReScanButton(_ sender: UIButton) {

        layoutSetupForBPScan()
        
        self.startBLEScan()
        
        scanButton.enable(isEnable: false)
    }
    
    
    @objc private func didClickPrintButton(_ sender: UIButton) {

        if let printView = printView {
            let image = UIImage.image(fromView: printView)
            share(image: image)
        }
    }
    
    private func share(image: UIImage) {
        let vc = UIActivityViewController(activityItems: [image], applicationActivities: [])
        present(vc, animated: true)
    }
    
    private func getCurrentTimeStamp() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy hh:mm:ss a"
        let dateString = dateFormatter.string(from: Date())
        return dateString
    }
}

extension BPHomeVC {
    //MARK: - QR Code Generator
    private func displayQRCodeImage(bpMACAddress: String) {
        
        guard let processedImage = generateQRCodeImage(from: bpMACAddress) else {
            self.showAlert(message: "Error in generate QR code image")
            self.printView = nil
            return
        }
        
        //remove the scan bp button and create the QR code image
        let _ = baseView.subviews.map({$0.removeFromSuperview()})
        
        let container: UIView = baseView
    
        
        //create the QR code as image and text below the image
        
        let printView = UIView()
        
        let imageView = UIImageView(image: processedImage)
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        printView.addSubview(imageView)
        
        NSLayoutConstraint.activate([
            imageView.widthAnchor.constraint(equalToConstant: 250),
            imageView.heightAnchor.constraint(equalToConstant: 250),
            imageView.topAnchor.constraint(equalTo: printView.topAnchor, constant: 0),
            imageView.centerXAnchor.constraint(equalTo: printView.centerXAnchor, constant: 0)
        ])
         
        
        let lblMACAddress = getLabel(valueString: "Urion BP MAC Address: " + bpMACAddress)
        
        lblMACAddress.translatesAutoresizingMaskIntoConstraints = false
        printView.addSubview(lblMACAddress)
        
        NSLayoutConstraint.activate([
            lblMACAddress.leadingAnchor.constraint(equalTo: printView.leadingAnchor, constant: 10),
            lblMACAddress.trailingAnchor.constraint(equalTo: printView.trailingAnchor, constant: -10),
            lblMACAddress.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 10)
        ])
        
        
        //add time stamp label
        if let currentDateString = getCurrentTimeStamp(), !currentDateString.isEmpty {
            
            let lblTimeStamp = getLabel(valueString: "Time Stamp: " + currentDateString)
            lblTimeStamp.textColor = .darkGray
            
            lblTimeStamp.translatesAutoresizingMaskIntoConstraints = false
            printView.addSubview(lblTimeStamp)
            
            NSLayoutConstraint.activate([
                lblTimeStamp.leadingAnchor.constraint(equalTo: printView.leadingAnchor, constant: 16),
                lblTimeStamp.trailingAnchor.constraint(equalTo: printView.trailingAnchor, constant: -10),
                lblTimeStamp.topAnchor.constraint(equalTo: lblMACAddress.bottomAnchor, constant: 10),
                lblTimeStamp.bottomAnchor.constraint(equalTo: printView.bottomAnchor, constant: -16)
            ])
        } else {
            NSLayoutConstraint.activate([
                lblMACAddress.bottomAnchor.constraint(equalTo: printView.bottomAnchor, constant: -16)
            ])
        }
        
        
        
        printView.translatesAutoresizingMaskIntoConstraints = false
        container.addSubview(printView)
        
        NSLayoutConstraint.activate([
            printView.leadingAnchor.constraint(equalTo: container.leadingAnchor, constant: 0),
            printView.trailingAnchor.constraint(equalTo: container.trailingAnchor, constant: 0),
            printView.topAnchor.constraint(equalTo: container.topAnchor, constant: 100)
        ])
        
        
        
        //create a button for Share the QR code image
        let printButton = getButton(title: "Print")
        printButton.addTarget(self, action: #selector(didClickPrintButton), for: .touchUpInside)
        
        printButton.translatesAutoresizingMaskIntoConstraints = false
        container.addSubview(printButton)
        
        NSLayoutConstraint.activate([
            printButton.widthAnchor.constraint(equalToConstant: 150),
            printButton.heightAnchor.constraint(equalToConstant: 40),
            printButton.topAnchor.constraint(equalTo: printView.bottomAnchor, constant: 20),
            printButton.trailingAnchor.constraint(equalTo: container.centerXAnchor, constant: -10)
        ])
        
        printButton.cornerRadius(radius: 20)
        
        
        //create a re scan button for BP Urion
        let reScanButton = getButton(title: "Re-Scan BP device")
        reScanButton.addTarget(self, action: #selector(didClickReScanButton), for: .touchUpInside)
        
        reScanButton.translatesAutoresizingMaskIntoConstraints = false
        container.addSubview(reScanButton)
        
        NSLayoutConstraint.activate([
            reScanButton.widthAnchor.constraint(equalToConstant: 170),
            reScanButton.heightAnchor.constraint(equalToConstant: 40),
            reScanButton.topAnchor.constraint(equalTo: printView.bottomAnchor, constant: 20),
            reScanButton.leadingAnchor.constraint(equalTo: container.centerXAnchor, constant: 10)
        ])
        
        reScanButton.cornerRadius(radius: 20)
        
        self.printView = printView
    }
    
    
    private func generateQRCodeImage(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)

        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 13, y: 13)

            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }

        return nil
    }
}


extension BPHomeVC {
    
    //MARK: - Bluetooth Permission
    private func initiateBLE() {
        let centralQueue: DispatchQueue = DispatchQueue(label: "com.iosbrain.centralQueueName", attributes: .concurrent)
        cbManager = CBCentralManager(delegate: self, queue: centralQueue, options: [CBCentralManagerOptionRestoreIdentifierKey: "bleDdeviceIntegration"])
    }
     
    //MARK: - BLE Delegates
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        switch central.state {
            case .unknown:
                print("Bluetooth status is unknown")
            case .resetting:
                print("Bluetooth status is resetting")
            case .unsupported:
                print("Bluetooth status is unsupported")
            case .unauthorized:
                print("Bluetooth status is unauthorized")
                DispatchQueue.main.async {
                    self.alertUser(message: "Your bluetooth access for Sim Card Manager app is unauthorized. Please authorize bluetooth access for Sim Card Manager in your phone setting to continue measurement.", actionButtonTitle: "Setting")
                }
            case .poweredOff:
                print("Bluetooth status is powered off")
                DispatchQueue.main.async {
                    self.showAlert(message: "Bluetooth Status is Off")
                }
            case .poweredOn:
                print("Scannig for BLE Devices...")
            
                //always reset the mac address
                self.startBLEScan()
            
                break
            @unknown default:
                break
        }
    }
    
    func centralManager(_ central: CBCentralManager, willRestoreState dict: [String : Any]) {
    }
    
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        
        if (peripheral.name == "Bluetooth BP" || peripheral.name == "Wileless BP" || peripheral.name == "Urion BP" || peripheral.name == "BLE to UART_2" || peripheral.name == "BLESmart_00000154EC21E58F1CC7" || peripheral.name == "BP7250") {

            if urionMACAddress == nil {
                if let manufactureData = advertisementData["kCBAdvDataManufacturerData"] as? Data {
                    let hexaString = manufactureData.hexEncodedString(options: .upperCase)
                    
                    self.urionMACAddress = hexaString
                    
                    if let urionMACAddress = self.urionMACAddress, !urionMACAddress.isEmpty {
                        DispatchQueue.main.async {
                            self.invaldiateTimer()
                            self.showActivityIndicator(message: "", show: false)
                            self.displayQRCodeImage(bpMACAddress: urionMACAddress)
                        }
                    }
                }
            }

            //once we found the urion device we can stop the scan
            if let macAddress = urionMACAddress, !macAddress.isEmpty {
                cbManager?.stopScan()
            }
        }
    }
    
    
    //MARK: - BLE Helper
    private func alertUser(message: String, actionButtonTitle: String) {
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: actionButtonTitle, style: .default) { (action) in
            if let url = URL(string:UIApplication.openSettingsURLString) {
               if UIApplication.shared.canOpenURL(url) {
                   UIApplication.shared.open(url, options: [:], completionHandler: nil)
               }
            }
        }
        alert.addAction(okAction)
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    private func startBLEScan() {
        //always reset the mac address
        self.urionMACAddress = nil
        
        cbManager?.scanForPeripherals(withServices: nil, options: [CBCentralManagerScanOptionAllowDuplicatesKey: false])
        
        DispatchQueue.main.async {
            self.showActivityIndicator(message: "Scanning", show: true)
            
            self.scanTimer = nil
            
            let date = Date().addingTimeInterval(30)
            self.scanTimer = Timer(fireAt: date, interval: 0, target: self, selector: #selector(self.scanningTime), userInfo: nil, repeats: false)
            
            if let scanTimer = self.scanTimer {
                RunLoop.main.add(scanTimer, forMode: .common)
            }
        }
        
    }
    
    @objc private func scanningTime() {
 
        cbManager?.stopScan()
        
        invaldiateTimer()
        
        self.showAlert(message: "No BP device found")
        
        self.scanButton.enable(isEnable: true)
        
        self.showActivityIndicator(message: "", show: false)
    }
    
    private func invaldiateTimer() {
        self.scanTimer?.invalidate()
        self.scanTimer = nil
    }
}
