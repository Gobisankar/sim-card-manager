//
//  BarCodeScannerVC.swift
//  SimCardManager
//
//  Created by Humworld Solutions Private Limited on 26/10/21.
//

import UIKit
import AVFoundation
import Vision

extension CIImage {
  func toUIImage() -> UIImage? {
    let context: CIContext = CIContext.init(options: nil)

    if let cgImage: CGImage = context.createCGImage(self, from: self.extent) {
      return UIImage(cgImage: cgImage)
    } else {
      return nil
    }
  }
}

class BarCodeScannerVC: UIViewController {

    
    private var avCaptureSession = AVCaptureSession()
    private var avPreviewLayer: AVCaptureVideoPreviewLayer?
    private var qrCodeFrameView: UIView?
    
    // TODO: Make VNDetectBarcodesRequest variable
    lazy var detectBarcodeRequest = VNDetectBarcodesRequest { request, error in
      guard error == nil else {
//        self.showAlert(withTitle: "Barcode error", message: error?.localizedDescription ?? "error")
        return
      }
      self.processClassification(request)
    }
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Scan Simcard"
//        setupVideoInputOutputDevice()
        setupCameraLiveView()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if avCaptureSession.isRunning {
            avCaptureSession.stopRunning()
        }
    }
    
    override func viewDidLayoutSubviews() {
        setupPreviewLayerFrame()
    }
    
    private func setupCameraLiveView() {
        avCaptureSession.sessionPreset = .hd1280x720

        let videoDevice = AVCaptureDevice
        .default(.builtInWideAngleCamera, for: .video, position: .back)

        guard
        let device = videoDevice,
        let videoDeviceInput = try? AVCaptureDeviceInput(device: device),
        avCaptureSession.canAddInput(videoDeviceInput) else {
        //          showAlert(
        //            withTitle: "Cannot Find Camera",
        //            message: "There seems to be a problem with the camera on your device.")
          return
        }

        avCaptureSession.addInput(videoDeviceInput)

        let captureOutput = AVCaptureVideoDataOutput()
        captureOutput.videoSettings = [kCVPixelBufferPixelFormatTypeKey as String: Int(kCVPixelFormatType_32BGRA)]
        captureOutput.setSampleBufferDelegate(self, queue: DispatchQueue.global(qos: DispatchQoS.QoSClass.default))
        avCaptureSession.addOutput(captureOutput)

        configurePreviewLayer()

        avCaptureSession.startRunning()
    }
    
    private func configurePreviewLayer() {
      let cameraPreviewLayer = AVCaptureVideoPreviewLayer(session: avCaptureSession)
      cameraPreviewLayer.videoGravity = .resizeAspectFill
      cameraPreviewLayer.connection?.videoOrientation = .portrait
      cameraPreviewLayer.frame = view.frame
      view.layer.insertSublayer(cameraPreviewLayer, at: 0)
    }
    
    func processClassification(_ request: VNRequest) {
      guard let barcodes = request.results else { return }
      DispatchQueue.main.async { [self] in
        if avCaptureSession.isRunning {
          view.layer.sublayers?.removeSubrange(1...)

          for barcode in barcodes {
              print("barcode", barcode) //VNRecognizedText
              let test = barcode as? VNBarcodeObservation
              print("barcodeDescriptor", test?.payloadStringValue)
            guard
              // TODO: Check for QR Code symbology and confidence score
              let potentialQRCode = barcode as? VNBarcodeObservation,
              potentialQRCode.symbology == .code128,
              potentialQRCode.confidence > 0.9
              else { return }

              print("potentialQRCode", potentialQRCode)
              
              if let iccidNumber = potentialQRCode.payloadStringValue, !iccidNumber.isEmpty, iccidNumber.count >= 18, iccidNumber.count <= 22 {
                  self.avCaptureSession.stopRunning()
                  NotificationCenter.default.post(name: .get_barcode_text, object: nil, userInfo: ["potentialQRCode": potentialQRCode.payloadStringValue ?? ""])
                  self.navigationController?.popViewController(animated: true)
              }
          }
        }
      }
    }

    //MARK: - Handler
    func observationHandler(payload: String?) {
      // TODO: Open it in Safari
      guard
        let payloadString = payload,
        let url = URL(string: payloadString),
        ["http", "https"].contains(url.scheme?.lowercased())
      else { return }

//      let config = SFSafariViewController.Configuration()
//      config.entersReaderIfAvailable = true
//
//      let safariVC = SFSafariViewController(url: url, configuration: config)
//      safariVC.delegate = self
//      present(safariVC, animated: true)
    }
    
    
    //MARK: - Logical Functions
    private func setupPreviewLayerFrame() {
        let bounds = view.layer.bounds
        avPreviewLayer?.position = CGPoint(x: bounds.midX, y: bounds.midY)
    }
    
    private func setupVideoInputOutputDevice() {
        
        //get the devices with camera using discovery session
//        let discoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInDualCamera], mediaType: .video, position: .back)
        let discoverySession = AVCaptureDevice.default(for: .video)
        
//        guard let captureDevice = discoverySession.devices.first else {
//            print("Failed to get device")
//            return
//        }
        
        //add the capture input device to the capture session
        do {
            let inputDevice = try AVCaptureDeviceInput(device: discoverySession!)
            avCaptureSession.addInput(inputDevice)
        } catch {
            print("device can not be added")
            return
        }
        
        //add the output to the capture session
        let captureMediaOutput = AVCaptureMetadataOutput()
        avCaptureSession.addOutput(captureMediaOutput)
        
        captureMediaOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        captureMediaOutput.metadataObjectTypes = [.ean8, .ean13, .pdf417, .code39, .code39Mod43, .code93, .code128, .upce, .itf14, .interleaved2of5, .aztec ]
        
        //add the preview layer to the app
        avPreviewLayer = AVCaptureVideoPreviewLayer(session: avCaptureSession)
        avPreviewLayer?.videoGravity = .resizeAspectFill
        avPreviewLayer?.frame = self.view.layer.bounds
        self.view.layer.addSublayer(avPreviewLayer!)
        
        setupPreviewLayerFrame()
        
        avCaptureSession.startRunning()
    }

}

extension BarCodeScannerVC: AVCaptureVideoDataOutputSampleBufferDelegate {
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
      // TODO: Live Vision
      guard let pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else { return }

      let imageRequestHandler = VNImageRequestHandler(
        cvPixelBuffer: pixelBuffer,
        orientation: .right)

      do {
        try imageRequestHandler.perform([detectBarcodeRequest])
      } catch {
        print(error)
      }
    }
}



extension BarCodeScannerVC: AVCaptureMetadataOutputObjectsDelegate {
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        
        if (metadataObjects.count == 0) {
            //No objects found so far
            print("NO QR Objects found so far")
            //qrCodeFrameView?.frame = .zero
            return
        }
        
        if let metaDataObject = metadataObjects.first {
            guard let readableObject = metaDataObject as? AVMetadataMachineReadableCodeObject else {
                return
            }
            print("readableObject type", readableObject.type)
            defer {
                setupPreviewLayerFrame()
            }
            //if readableObject.type == AVMetadataObject.ObjectType.qr {
                
                let barCodeObject = avPreviewLayer?.transformedMetadataObject(for: readableObject)
                self.view.frame = barCodeObject!.bounds
                
                guard let stringValue = readableObject.stringValue else {
                    return
                }
                avCaptureSession.stopRunning()
                AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
                print("found QR Code", stringValue)
//                if deviceType == .GATEWAY {
//                    processGatewayQRCode(qrCode: stringValue)
//                } else {
//                    processKioskQRCode(qrCode: stringValue)
//                }
//            } else {
//                avCaptureSession.stopRunning()
//                self.handleInvalidQRCode(isInvalid: true)
//            }
        }
    }
    
    private func processKioskQRCode(qrCode: String) {
        guard qrCode != "" && qrCode.count == 16 else {
            self.handleInvalidQRCode(isInvalid: true)
            return
        }
        
//        let validQRCode = qrCode.range(of: REGEX_KIOSK_QR_CODE, options: .regularExpression)
//        if validQRCode != nil {
//            NotificationCenter.default.post(name: .getQRCode, object: nil, userInfo: ["serailNumber": qrCode, "deviceType": QRCodeDeviceType.KIOSK.rawValue])
//            self.navigationController?.popViewController(animated: true)
//        } else {
//            self.handleInvalidQRCode(isInvalid: true)
//        }
    }
    
    private func processGatewayQRCode(qrCode: String) {
        
        guard qrCode != "" && qrCode.count == 22 else {
            self.handleInvalidQRCode(isInvalid: true)
            return
        }
        let qrCodeArray = qrCode.split(separator: "|")
        let rawSerailNumber: String = String(qrCodeArray[0])
        let serailNumber = rawSerailNumber.replacingOccurrences(of: ":", with: "")
        //do the service call for validation and save
        guard serailNumber.count == 12 else {
            self.handleInvalidQRCode(isInvalid: true)
            return
        }
        
//        NotificationCenter.default.post(name: .getQRCode, object: nil, userInfo: ["serailNumber": serailNumber, "deviceType": QRCodeDeviceType.GATEWAY.rawValue])
        self.navigationController?.popViewController(animated: true)
    }
    
    private func handleInvalidQRCode(isInvalid: Bool) {
        var message = "Invalid QR Code"
        if isInvalid == false {
            message = "Timeout"
        }
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        let reScanAction = UIAlertAction(title: "Rescan", style: .default) { (rescan) in
            self.setupPreviewLayerFrame()
            self.avCaptureSession.startRunning()
        }
        let cancel = UIAlertAction(title: "Cancel", style: .destructive) { (cancel) in
            self.navigationController?.popViewController(animated: true)
        }
        alertController.addAction(cancel)
        alertController.addAction(reScanAction)
        self.present(alertController, animated: true, completion: nil)
    }
}


