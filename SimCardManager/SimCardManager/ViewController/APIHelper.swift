//
//  APIHelper.swift
//  SimCardManager
//
//  Created by Humworld Solutions Private Limited on 27/10/21.
//

import Foundation

extension Notification.Name {
    static let get_barcode_text = Notification.Name("get_barcode_text")
    static let get_gateway_barcode = Notification.Name("get_gateway_barcode")
}

struct Constant {
    static let baseURL = "https://apicontrolcenter.com/"
    static let login = "auth/token"
    static let fetchSimCardDetailsURL = "assets/"
    static let fetchProductsURL = "/products"
    static let suspend = "/suspend"
    static let unsuspend = "/unsuspend"
    static let subscribe = "/subscribe"
    static let accountId = "9927232d-a4de-42f8-b6bb-e384f4353bf6"
    static let userName = "Venkataraman Soundararajan"
    static let password = "sudhir123"
    static let defaultPlanName = "Humworld_AT&T_10MB@0.82USD_ovr@0.10usd"
    static let defaultPlanNameTwo = "Humworld_ROTAM_4MB@0.54USD_ovr0.05USD"
    //static let mapSimCardURL = "https://testapi.humhealth.com/HumHealthTestingAPI/update/iccid"
    static let mapSimCardURL = "https://api.humhealth.com/HumHealthWebAPI/update/iccid"
}

class APIHelper: NSObject {
    
    static let sharedInstance: APIHelper = {
        let instance = APIHelper()
        // setup code
        return instance
    }()
    
    
    
    func webserviceDataParams(serviceURL: String, params: Data?, httpMethod: String, completionHandler: @escaping (_ data: Data?, _ error: Error?) -> ()) {
        let serviceURL = URL(string: serviceURL)
        var request = URLRequest(url: serviceURL!, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 300)
        
        if let token: String = UserDefaults.standard.object(forKey: "podgrouptoken") as? String {
            request.addValue(token, forHTTPHeaderField: "x-access-token")
        }
        
        if let data = params {
            request.httpBody = data
        }
        
        request.httpMethod = httpMethod
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("no-cache", forHTTPHeaderField: "Cache-Control")
        request.httpShouldHandleCookies = false
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (data, response, error) in
            
            guard error == nil else {
                completionHandler(nil, error)
                return
            }
            
            let httpResponse = response as? HTTPURLResponse
            let statusCode = httpResponse?.statusCode
            print("status code", statusCode ?? 000)
            guard 200...299 ~= statusCode! else {
                completionHandler(nil, error)
                return
            }
            completionHandler(data, nil)
        }
        task.resume()
    }
    
    
    func webserviceQueryParamsMethod(serviceURL: String, params: [String: String], httpMethod: String, completionHandler: @escaping (_ data: Data?, _ error: Error?) -> ()) {
        
        var urlComponents: URLComponents = URLComponents(string: serviceURL)!
        urlComponents.queryItems = params.map { (key, value) in
                URLQueryItem(name: key, value: value)
        }
        var request = URLRequest(url: urlComponents.url!, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 300)
        
        if let token: String = UserDefaults.standard.object(forKey: "podgrouptoken") as? String {
            request.addValue(token, forHTTPHeaderField: "x-access-token")
        }
        
        request.httpMethod = httpMethod
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("no-cache", forHTTPHeaderField: "Cache-Control")
        request.httpShouldHandleCookies = false
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (data, response, error) in
            
            guard error == nil else {
                completionHandler(nil, error)
                return
            }
            
            let httpResponse = response as? HTTPURLResponse
            let statusCode = httpResponse?.statusCode
            print("status code", statusCode ?? 000)
            guard 200...299 ~= statusCode! else {
                completionHandler(nil, error)
                return
            }
            completionHandler(data, nil)
        }
        task.resume()
    }
    
}

